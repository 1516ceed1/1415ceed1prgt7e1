package org.ceedcv.ceed1prgt7e1.controlador;

import java.awt.Desktop;
import static java.awt.SystemColor.desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

import org.ceedcv.ceed1prgt7e1.modelo.IModelo;
import org.ceedcv.ceed1prgt7e1.vista.VistaGrafica;
import org.ceedcv.ceed1prgt7e1.vista.VistaGraficaAcerca;
import org.ceedcv.ceed1prgt7e1.vista.VistaGraficaAlumno;
import org.ceedcv.ceed1prgt7e1.vista.VistaGraficaGrupo;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Controlador implements ActionListener {

   IModelo modelo;

   VistaGrafica vista;
   VistaGraficaGrupo vistaGrupo;
   VistaGraficaAlumno vistaAlumno;
   VistaGraficaAcerca jpanelAcerca;

   JFrame jframeGrupo;
   JFrame jframeAlumno;
   JFrame jframeAcerca;

   ControladorAlumno controladorAlumno;
   ControladorGrupo controladorGrupo;
   ControladorAcerca controladorAcerca;

   final static String ALUMNO = "ALUMNO";
   final static String GRUPO = "GRUPO";
   final static String DOCU = "DOCUMENTACIÓN";
   final static String ACERCA = "ACERCA";
   final static String SALIR = "SALIR";

   Controlador(IModelo modelo_, VistaGrafica vista_) {
      vista = vista_;
      modelo = modelo_;
      inicializa();
   }

   @Override
   public void actionPerformed(ActionEvent e) {

      String comando = e.getActionCommand();

      switch (comando) {
         case SALIR:
            salir();
            break;
         case ALUMNO:
            alumno();
            break;
         case GRUPO:
            grupo();
            break;
         case DOCU:
            web();
            break;
         case ACERCA:
            acerca();
            break;
      }

   }

   private void salir() {
      vista.setVisible(false);
      System.exit(0);
   }

   private void inicializa() {

      vista.getjbAlumno().setActionCommand(ALUMNO);
      vista.getjbGrupo().setActionCommand(GRUPO);
      vista.getjbDocu().setActionCommand(DOCU);
      vista.getjbAcerca().setActionCommand(ACERCA);
      vista.getjbSalir().setActionCommand(SALIR);

      vista.getjbAlumno().addActionListener(this);
      vista.getjbGrupo().addActionListener(this);
      vista.getjbDocu().addActionListener(this);
      vista.getjbAcerca().addActionListener(this);
      vista.getjbSalir().addActionListener(this);

      vista.setTitle("VISTA PRINCIPAL");
      vista.setLocationRelativeTo(null); // Centrar
      vista.setVisible(true);
   }

   private void alumno() {
      vistaAlumno = new VistaGraficaAlumno();
      vistaAlumno.setTitle(ALUMNO);
      controladorAlumno = new ControladorAlumno(vistaAlumno, modelo);
   }

   private void grupo() {
      vistaGrupo = new VistaGraficaGrupo();
      vistaGrupo.setTitle(GRUPO);
      controladorGrupo = new ControladorGrupo(vistaGrupo, modelo);

   }

   private void acerca() {
      jpanelAcerca = new VistaGraficaAcerca();
      jpanelAcerca.setTitle(ACERCA);
      controladorAcerca = new ControladorAcerca(jframeAcerca, jpanelAcerca);
   }

   private void pdf() {
      try {
         String filename = "docu.pdf";
         File doc = new File(filename);
         Desktop.getDesktop().open(doc);
      } catch (IOException ex) {
         Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
      }
   }

   private void web() {

      String url = "https://docs.google.com/document/d/1pMHwoJj7_NjvooKJubFBl9eGE5t7wBHQi2BBPuGEUkg/edit?pref=2&pli=1";

      Desktop desktop = Desktop.getDesktop();

      if (desktop.isSupported(Desktop.Action.BROWSE)) {
         try {
            desktop.getDesktop().browse(new URI(url));
         } catch (URISyntaxException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
         }
      } else {
         pdf();
      }
   }

}
