/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt7e1.controlador;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JFrame;
import org.ceedcv.ceed1prgt7e1.modelo.Alumno;
import org.ceedcv.ceed1prgt7e1.modelo.IModelo;

import org.ceedcv.ceed1prgt7e1.vista.VistaGraficaAcerca;

/**
 *
 * @author paco
 */
class ControladorAcerca implements ActionListener {

   final static String SALIR = "Salir";
   VistaGraficaAcerca vista;
   JFrame jframe;

   public ControladorAcerca(JFrame jframe, VistaGraficaAcerca vista) {

      this.vista = vista;
      this.jframe = jframe;
      inicializa();
   }

   ControladorAcerca(VistaGraficaAcerca vista) {
      this.vista = vista;
   }

   private void salir() {
      vista.setVisible(false);
   }

   private void inicializa() {
      vista.getjbSalir().setSize(new Dimension(100, 200));
      vista.getjbSalir().setText(SALIR);
      vista.getjbSalir().setActionCommand(SALIR);
      vista.getjbSalir().addActionListener(this);
      vista.setTitle("ACERCA DE");
      vista.setLocationRelativeTo(null); // Centrar
      vista.setVisible(true);
   }

   @Override
   public void actionPerformed(ActionEvent e) {
      String comando = e.getActionCommand();

      switch (comando) {
         case SALIR:
            salir();
            break;
      }
   }

}
