/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt7e1.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JFrame;
import org.ceedcv.ceed1prgt7e1.modelo.Alumno;
import org.ceedcv.ceed1prgt7e1.modelo.Grupo;
import org.ceedcv.ceed1prgt7e1.modelo.IModelo;
import org.ceedcv.ceed1prgt7e1.vista.Funciones;
import org.ceedcv.ceed1prgt7e1.vista.Verificador;
import org.ceedcv.ceed1prgt7e1.vista.VistaGraficaAlumno;

/**
 * @author paco
 */
public class ControladorAlumno implements ActionListener {

    JFrame jframe;
    VistaGraficaAlumno vista;
    IModelo modelo;
    Alumno actual;
    int iactual;
    ArrayList alumnos;
    String operacion = ""; // Se guarda la ultima operacion realizada

    // Operaciones: CRUD
    final static String ACREATE = "CREATE";
    final static String AREAD = "READ";
    final static String AUPDATE = "UPDATE";
    final static String ADELETE = "DELETE";
    final static String ASALIR = "SALIR";

    final static String APRIMERO = "PRIMERO";
    final static String AULTIMO = "ULTIMO";
    final static String ASIGUIENTE = "SIGUIENTE";
    final static String AANTERIOR = "ANTERIOR";

    final static String AGUARDAR = "GUARDAR";
    final static String ACANCELAR = "CANCELAR";

    ControladorAlumno(VistaGraficaAlumno vista, IModelo modelo) {

        this.vista = vista;
        this.modelo = modelo;
        inicializa();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Alumno alumno;

        switch (comando) {

            case APRIMERO:
                primero();
                mostrar(actual);
                break;

            case AULTIMO:
                ultimo();
                mostrar(actual);
                break;

            case ASIGUIENTE:
                siguiente();
                mostrar(actual);
                break;

            case AANTERIOR:
                anterior();
                mostrar(actual);
                break;

            case ASALIR: // Exit
                salir();
                break;

            case ACREATE: // Create
                operacion = ACREATE;
                editarCampos(true);
                activaCRUD(false);
                rellenarCombobox(null);
                vista.getjtfId().setText("");
                vista.getjtfId().setEnabled(false);
                verificar();
                break;

            case AGUARDAR:
                // Analiza la ultima operacion realizada
                switch (operacion) {
                    case ACREATE:
                        alumno = obtener();
                        modelo.create(alumno);
                        alumnos.add(alumno);
                        actual = alumno;
                        iactual = alumnos.size() - 1; // Añade por el final
                        break;
                    case ADELETE:
                        alumno = obtener();
                        modelo.delete(alumno);
                        alumnos.remove(iactual);
                        anterior();
                        break;
                    case AUPDATE:  // Actualizar
                        alumno = obtener();
                        modelo.update(alumno);
                        actual = alumno;
                        alumnos = modelo.reada();
                        break;
                    case AREAD:
                        alumno = obtenerId();
                        String id = alumno.getId();
                        getAlumno(id);
                        break;
                }
                mostrar(actual);
                operacion = "";
                break;
            // Fin Guardar

            case ACANCELAR:
                mostrar(actual);
                operacion = "";
                break;

            case AREAD: // Read
                operacion = AREAD;
                vista.getjtfId().setText("");
                vaciarCampos();
                editarCampos(false);
                activaCRUD(false);
                vista.getjtfId().setEditable(true); // Pedimos solo la id.

                break;

            case AUPDATE:  // Actualizar
                operacion = AUPDATE;
                editarCampos(true);
                activaCRUD(false);
                verificar();
                break;

            case ADELETE: // Borrar
                operacion = ADELETE;
                activaCRUD(false);
                break;

        }
    }

    public void vaciarCampos() {
        vista.getjtfId().setText("");
        vista.getjtfNombre().setText("");
        vista.getjtfEdad().setText("");
        vista.getjtfEmail().setText("");
        vista.getjcbGrupo().addItem("");
    }

    public void editarCampos(Boolean b) {
        vista.getjtfId().setEditable(b);
        vista.getjtfNombre().setEditable(b);
        vista.getjtfEmail().setEditable(b);
        vista.getjtfEdad().setEditable(b);
        vista.getjcbGrupo().setEnabled(b);
        vista.getjdcFecha().setEnabled(b);
    }

    public void activaCRUD(Boolean b) {
        vista.getjbGuardar().setEnabled(!b);
        vista.getjbCancelar().setEnabled(!b);
        vista.getjbSalir().setEnabled(b);
        vista.getjbCreate().setEnabled(b);
        vista.getjbRead().setEnabled(b);
        vista.getjbUpdate().setEnabled(b);
        vista.getjbDelete().setEnabled(b);
    }

    private void salir() {
        vista.setVisible(false);
    }

    private void inicializa() {
        //vistaprincipal.alumno().setActionCommand(ALUMNO);

        vista.getjbPrimero().setActionCommand(APRIMERO);
        vista.getjbUltimo().setActionCommand(AULTIMO);
        vista.getjbSiguiente().setActionCommand(ASIGUIENTE);
        vista.getjbAnterior().setActionCommand(AANTERIOR);
        vista.getjbGuardar().setActionCommand(AGUARDAR);
        vista.getjbCancelar().setActionCommand(ACANCELAR);
        vista.getjbCreate().setActionCommand(ACREATE);
        vista.getjbRead().setActionCommand(AREAD);
        vista.getjbUpdate().setActionCommand(AUPDATE);
        vista.getjbDelete().setActionCommand(ADELETE);
        vista.getjbSalir().setActionCommand(ASALIR);

        vista.getjbPrimero().addActionListener(this);
        vista.getjbUltimo().addActionListener(this);
        vista.getjbSiguiente().addActionListener(this);
        vista.getjbAnterior().addActionListener(this);

        vista.getjbGuardar().addActionListener(this);
        vista.getjbCancelar().addActionListener(this);

        vista.getjbCreate().addActionListener(this);
        vista.getjbRead().addActionListener(this);
        vista.getjbUpdate().addActionListener(this);
        vista.getjbDelete().addActionListener(this);
        vista.getjbSalir().addActionListener(this);

        vista.setTitle("ALUMNO");
        vista.setLocationRelativeTo(null); // Centrar
        vista.setVisible(true);

        alumnos = modelo.reada();
        if (alumnos.size() > 0) {
            primero();
        }
        mostrar(actual);

    }

    private void mostrar(Alumno primero) {

        editarCampos(false);
        activaCRUD(true);

        if (primero == null) {
            return;
        }

        rellenarCombobox(primero);

        if (primero == null) {
            return;
        }

        vista.getjtfId().setText(primero.getId());
        vista.getjtfNombre().setText(primero.getNombre());
        vista.getjtfEdad().setText(primero.getEdad() + "");
        vista.getjtfEmail().setText(primero.getEmail());
        vista.getjdcFecha().setDate(primero.getFecha());

    }

    private Alumno obtenerId() {
        Alumno alumno = new Alumno();
        alumno.setId(vista.getjtfId().getText());
        return alumno;
    }

    private Alumno obtener() {
        Alumno alumno = new Alumno();
        String edad = "0";

        alumno.setId(vista.getjtfId().getText());
        alumno.setNombre(vista.getjtfNombre().getText());

        try {
            alumno.setEdad(Integer.parseInt(vista.getjtfEdad().getText()));
        } catch (NumberFormatException nfe) {
            alumno.setEdad(0);
        }

        alumno.setEmail(vista.getjtfEmail().getText());

        String anyo = Integer.toString(vista.getjdcFecha().getCalendar().get(java.util.Calendar.YEAR));
        String mes = Integer.toString(vista.getjdcFecha().getCalendar().get(java.util.Calendar.MONTH) + 1);
        String dia = Integer.toString(vista.getjdcFecha().getCalendar().get(java.util.Calendar.DATE));

        Date fecha = vista.getjdcFecha().getDate();
        //String sfecha = dia + "-" + mes + "-" + anyo;
        //Funciones f = new Funciones();
        //fecha = f.ConvertirStringaDate(sfecha);
        alumno.setFecha(fecha);

        Grupo grupo = new Grupo();
        Object[] sgrupo = null;
        if (vista.getjcbGrupo().getSelectedObjects().length > 0) {
            sgrupo = vista.getjcbGrupo().getSelectedObjects();
            alumno.setGrupo((Grupo) sgrupo[0]);
        }

        return alumno;

    }

    private void getAlumno(String id) {

        Boolean encontrado = false;
        Alumno alumno = null;

        int talla = alumnos.size();
        int i = 0;

        while (i < talla && encontrado == false) {
            alumno = (Alumno) alumnos.get(i);
            if (alumno.getId().equals(id)) {
                encontrado = true;
                iactual = i;
            }
            i++;
        }

        if (encontrado == true) {
            actual = alumno;
        } else {
            iactual = 0;
            actual = (Alumno) alumnos.get(iactual);
        }
    }

    private void anterior() {
        if (iactual != 0) {
            iactual--;
            actual = (Alumno) alumnos.get(iactual);
        }
    }

    private void siguiente() {
        if (iactual != alumnos.size() - 1) {
            iactual++;
            actual = (Alumno) alumnos.get(iactual);
        }
    }

    private void ultimo() {
        iactual = alumnos.size() - 1;
        actual = (Alumno) alumnos.get(iactual);
    }

    private void primero() {

        if (alumnos != null) {
            actual = (Alumno) alumnos.get(iactual);
            iactual = 0;
        } else {
            actual = null;
            iactual = -1;
        }
    }

    private void verificar() {
        vista.getjtfEdad().setInputVerifier(new Verificador("Edad"));
        vista.getjtfEmail().setInputVerifier(new Verificador("Email"));
    }

    private void rellenarCombobox(Alumno alumno) {

        ArrayList grupos = modelo.readg();
        if (grupos != null) {
            vista.getjcbGrupo().removeAllItems();
            vista.getjcbGrupo().addItem("No existe");

            for (int i = 0; i < grupos.size(); i++) {
                vista.getjcbGrupo().addItem(grupos.get(i));

                Grupo grupo = (Grupo) grupos.get(i);

                if (alumno != null && alumno.getGrupo() != null) {
                    if (grupo.getId().equals(alumno.getGrupo().getId())) { // Coincide
                        vista.getjcbGrupo().setSelectedItem(grupos.get(i));

                    }
                }
            }
        }
    }

}
