package org.ceedcv.ceed1prgt7e1.controlador;

import java.io.IOException;
import org.ceedcv.ceed1prgt7e1.modelo.IModelo;
import org.ceedcv.ceed1prgt7e1.modelo.ModeloFichero;
import org.ceedcv.ceed1prgt7e1.vista.VistaGrafica;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 22-feb-2016
 */
public class Main {

    public static void main(String[] args) throws IOException {

        IModelo modelo = new ModeloFichero();
        VistaGrafica vista = new VistaGrafica();
        Controlador controlador = new Controlador(modelo, vista);

    }

}
