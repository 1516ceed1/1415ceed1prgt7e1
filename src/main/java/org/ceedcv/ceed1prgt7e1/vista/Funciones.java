/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt7e1.vista;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paco Aldarias<paco.aldarias@ceedcv.es>
 */
public class Funciones {

   public Date ConvertirStringaDate(String strFecha) {
      Date fecha = null;
      SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
      try {
         fecha = formatoDelTexto.parse(strFecha);
      } catch (ParseException ex) {
         Logger.getLogger(Funciones.class.getName()).log(Level.SEVERE, null, ex);
      }
      return fecha;
   }

}
